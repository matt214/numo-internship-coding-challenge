import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import axios from 'axios';
import { render } from '@testing-library/react';
import App from './App';


describe('app component', () => {
  jest.mock('axios')
  configure({adapter: new Adapter()});
  describe('when rendered', () => {
    it('should make a boredAPI call on render', () => {
      let getSpy = jest.spyOn(axios, 'get');
       let appInstance = shallow(
         <App/>
         );
        expect(getSpy).toBeCalled();
    });
  });
});

test('renders app correctly', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText('Are You Bored? Try This Activity...');
  expect(linkElement).toBeInTheDocument();
});

test('drop down menu renders', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText('social');
  expect(linkElement).toBeInTheDocument();
});