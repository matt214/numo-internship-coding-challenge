import React from 'react';
import './App.css';
import axios from "axios";

class App extends React.Component {
  constructor() {
    super();
    this.newActivity();
    this.state = {
      activities: []
    };
  }
  newActivity = () => {
    let abortController = new AbortController();
    axios.get(
      "http://www.boredapi.com/api/activity/"
    )
    .then(response => {
      if (response && response.data ) {
        this.setState({ activities: response.data });
        console.log(this.state.activities)
      }
    })
    .catch(error => {
      console.log("***** error: " , error);
    });
    abortController.abort();
  }
  handleSubmit = (event) => {
    event.preventDefault();
    const pplData = new FormData(event.target);
    const numOfPeople = pplData.get('numOfPeople');
    this.newByParticipants(numOfPeople);
  }
  typeSubmit = (event) => {
    const typeData = event.target.value;
    console.log(typeData)
    this.newByType(typeData);
  }
  newByType = (props) => {
    let abortController = new AbortController();
    axios.get(
      "http://www.boredapi.com/api/activity?type=" + props
    )
    .then(response => {
      if (response && response.data ) {
        this.setState({ activities: response.data });
        console.log(this.state.activities)
      }
    })
    .catch(error => {
      console.log("***** error: " , error);
    });
    abortController.abort();
  }
  newByParticipants = (props) => {
    let abortController = new AbortController();
    axios.get(
      "http://www.boredapi.com/api/activity?participants=" + props
    )
    .then(response => {
      if (response && response.data ) {
        this.setState({ activities: response.data });
        console.log(this.state.activities)
      }
    })
    .catch(error => {
      console.log("***** error: " , error);
    });
    abortController.abort();
  } 
  render() {
    return (
      <div id="app" className="app">
      <div id="activityCard" className="activity">
        <h4 className="top-text">Are You Bored? Try This Activity...</h4>
        <div className="description">{this.state.activities.activity}
        <p className="participants sub-type">Participants: {this.state.activities.participants} | 
        <span className="category sub-type"> Category: {this.state.activities.type}</span></p>
        </div>
      </div>
      <p className="or">Not your speed?</p>
        <button onClick={this.newActivity}>Click to find a new activity!</button>
        <form onSubmit={this.handleSubmit}>
          <p className="or">OR</p>
          <label>
            How many people are bored right now? <br></br>We've got activities for one to eight participants
            <br></br> <span className="or">Enter Here: </span><input type="text" name="numOfPeople" pattern="[1-8]" id="ppl" defaultValue="1" />
          </label>
          <input className="submit" type="submit" value="SUBMIT"  />
        </form>
        <div>
        <p className="or" >OR</p>
          <h4>Select an activity category: </h4>
          <select className="select-css" defaultValue="" onChange={this.typeSubmit}>
            <option id="ed" value="education">education</option>
            <option id="rec" value="recreational">recreational</option>
            <option id="social" value="social">social</option>
            <option id="diy" value="diy">diy</option>
            <option id="charity" value="charity">charity</option>
            <option id="relax" value="relaxation">relaxation</option>
            <option id="music" value="music">music</option>
            <option id="busy" value="busywork">busywork</option>
            <option id="cook" value="cooking">cooking</option>
          </select>
        </div>
      </div>
    );
  }
}

export default App;
